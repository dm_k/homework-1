//
//  HWConverterViewController.h
//  Homework1
//
//  Created by Vladislav Grigoriev on 29/09/16.
//  Copyright © 2016 com.inostudio.ios.courses. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HWHomeworkChildController.h"

@protocol HWConverterViewControllerDelegate;

@interface HWConverterViewController : UIViewController <HWHomeworkChildController>

@property (nonatomic, weak) id<HWConverterViewControllerDelegate> delegate;

- (void)setConvertedString:(NSString *)convertedString;

@end

@protocol HWConverterViewControllerDelegate <NSObject>

@required
- (void)converterController:(HWConverterViewController *)controller needConvertString:(NSString *)string;

@end
