//
//  HWHomework.c
//  Homework1
//
//  Created by Vladislav Grigoriev on 21/09/16.
//  Copyright © 2016 com.inostudio.ios.courses. All rights reserved.
//

#include "HWHomework.h"
#include "stdlib.h"
#include <math.h>
#include <time.h>
#include <string.h>

/*
 * Задание 1. (1 балл)
 *
 * Необходмо реализовать метод, который создаст матрицу и заполнит её случайными числами от -100 до 100.
 *
 * Параметры:
 *      rows - количество строк в матрице;
 *      columns - количество столбцов в матрице;
 *
 * Возвращаемое значение:
 *      Матрица размером [rows, columns];
 */
long** createMatrix(const long rows, const long columns) {
    long **arr = malloc(rows*sizeof(long*));
    srand((int)time(NULL));
    for(int i = 0; i < rows; i++)
    {
        arr[i] = malloc(columns*sizeof(long));
        for(int j = 0; j < columns; j++)
        {
            arr[i][j] = rand() % 200 - 100;
        }
    }
    return arr;
}

/*
 * Задание 2.
 *
 * Необходимо реализовать метод, котрый преобразует входную строку, состоящую из чисел, в строку состоящую из числительных.
 * Задание имеет два уровня сложности: обычный и высокий.
 *
 * Параметры:
 *      numberString - входная строка (например: "1", "123.456");
 *
 * Возвращаемое значение:
 *      Обычный уровень сложности (1 балл):
 *             Строка, содержащая в себе одно числительное или сообщение о невозможности перевода;
 *             Например: "один", "Невозможно преобразовать";
 *
 *      Высокий уровень сложности (2 балла):
 *              Строка, содержащая себе числительные для всех чисел или точку;
 *              Например: "один", "один два три точка четыре пять шесть";
 *
 *
 */
char* stringForNumberString(const char *numberString) {
    long length = strlen(numberString);
    char *result = malloc(sizeof(char[1024]));
    for(int i = 0; i < length; i++)
    {
        switch (numberString[i]) {
            case '1':
                sprintf(result, "%s%s", result, "один ");
                break;
                
            case '2':
                sprintf(result, "%s%s", result, "два ");
                break;
                
            case '3':
                sprintf(result, "%s%s", result, "три ");
                break;
                
            case '4':
                sprintf(result, "%s%s", result, "четыре ");
                break;
                
            case '5':
                sprintf(result, "%s%s", result, "пять ");
                break;
                
            case '6':
                sprintf(result, "%s%s", result, "шесть ");
                break;
                
            case '7':
                sprintf(result, "%s%s", result, "семь ");
                break;
                
            case '8':
                sprintf(result, "%s%s", result, "восемь ");
                break;
                
            case '9':
                sprintf(result, "%s%s", result, "девять ");
                break;
                
            case '0':
                sprintf(result, "%s%s", result, "ноль ");
                break;
                
            case '.':
                sprintf(result, "%s%s", result, "точка ");
                break;
                
            default:
                sprintf(result, "%s%s", result, "<Невозможно преобразовать!>");
                return result;
        }
    }
    return result;
}


/*
 * Задание 3.
 *
 * Необходимо реализовать метод, который возвращает координаты точки в зависимости от времени с начала анимации.
 * Задание имеет два уровня сложности: обычный и высокий.
 *
 * Параметры:
 *      time - время с начала анимации в секундах (например: 0 или 1.234);
 *      canvasSize - длина стороны квадрата, на котором происходит рисование (например: 386);
 *
 * Возвращаемое значение:
 *      Структура HWPoint, содержащая x и y координаты точки;
 *
 * Особенность:
 *      Начало координат находится в левом верхнем углу.
 *
 * Обычный уровень сложности (2 балла):
 *      Анимация, которая должна получиться представлена в файле Default_mode.mov.
 *
 * Высокий уровень сложности (3 балла):
 *      Анимация, которая должна получиться представлена в файле Hard_mode.mov.
 *
 * PS: Не обязательно, чтобы ваша анимация один в один совпадала с примером.
 * PPS: Можно реализовать свою анимацию, уровень сложности которой больше или равен анимациям в задании.
 */
HWPoint pointForAnimationTime(const double time, const double canvasSize) {
    double x, y, p, fi;
    int g;
    int speed = canvasSize / 2.0;
    int delta = (canvasSize > 343) ? 40 : 36.5; // only for iPhone 6 or 7 (s/Plus)
    
    x = time;
    p = 2*exp(x*0.5);
    fi = p;
    
    if(p < 400.0) { // first animation
        if(fi > 200) {
            g = fi/200;
            while(fi >= 200*g) {
                fi = fi - 200;
            }
            p = p - 1.5*fi;
        }
        
        y = p*sin(x*5) + canvasSize / 2.0;
        x = p*cos(x*5) + canvasSize / 2.0;
    }
    else { // two animation
        x = time * speed;
        
        if(x > canvasSize) {
            while(x >= canvasSize) {
                x -= canvasSize;
            }
        }
        
        y = 90*sin(x/delta)+speed-80;
        
        if((x <= canvasSize / 4.5) || (x >= canvasSize - canvasSize / 4.5)) {
            y = speed;
        }
    }
    
    return (HWPoint){x, y};
}
